'use strict';
const Sequelize = require('sequelize');
const config = require('config');

const connection = config.get("db.mysql");

const db = new Sequelize(connection.dbname, connection.user, connection.password, {
  pool: {maxConnections: connection.pool},
  dialect: 'mysql'
});

db.models.Users = db.import('../models/users');

module.exports = db;
