const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const router = express.Router();

const app = express();
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cookieParser());

const config = require('config');

app.use(router);

const services = require('../services/Users');

// route to get list of users
router
  .get('/list/:page/:limit', async (req, res) => {
    try {
      const {page, limit} = req.params;
      const query = req.query;

      const sort = {
        columnName: 'fullName',
        order: 'ASC'
      };

      if (query.sort) {
        sort.columnName = query.sort;
      }

      if (query.order) {
        sort.order = query.order;
      }

      const users = await services.listUsers(Number(page), Number(limit), sort);

      return res.status(200).json(users);
    }
    catch (error) {
      res.status(500).json(error);
    }
  });

// route to add new user
router
  .post('/', async (req, res, next) => {
    try {
      const {body} = req;
      const user = await services
        .addUser(body);

      return res.status(201).json(user);
    }
    catch(error) {
      res.status(500).json(error);
    }
  });

module.exports = app;
