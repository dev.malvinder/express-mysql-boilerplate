const express = require('express');
const path = require('path');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');
const config = require('config');

const r_api = require('../routes/api');
const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));

app.use(cookieParser());
app.use('/api', r_api);


// catch 404 and forward to error handler
/*app.use(function(req, res, next) {
  const err = new Error('Not Found');
  err.status = 404;
  next(err);
});*/

// error handlers
app.use(function(err, req, res, next) {
  res.status(err.status || 500);
  /*res.render('error', {
    message: err.message,
    error: err
  });*/
  console.error(err);
});

module.exports = app;
