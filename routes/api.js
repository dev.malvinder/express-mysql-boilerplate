const express = require('express');
const cookieParser = require('cookie-parser');
const bodyParser = require('body-parser');

const apiApp = new express();

const r_user = require('../controllers/Users');

apiApp.use(bodyParser.json());
apiApp.use(bodyParser.urlencoded({ extended: true }));
apiApp.use(cookieParser());


apiApp.get("/", (req, res, next) => {
  const msg = {};
  msg.status = "SUCCESS";
  msg.message = "APIs are working awesomely fine";
  msg.data = [];

  res.status(200).json(msg);
});

apiApp.use('/users', r_user);


module.exports = apiApp;
