const path = require('path');

const db = require('../helpers/db');

const {Users} = db.models;

/*
Roles.hasMany(Users, { foreignKey: 'roleId' });
Users.belongsTo(Roles, { foreignKey: 'roleId' });

Users.hasMany(UserDevices, { foreignKey: 'userId' });
UserDevices.belongsTo(Users, { foreignKey: 'userId' });

Users.hasOne(UserKeys, { foreignKey: 'userId' });
UserKeys.belongsTo(Users, { foreignKey: 'userId' });

Users.hasOne(Wallet, { foreignKey: 'userId' });
Wallet.belongsTo(Users, { foreignKey: 'userId' });

Users.hasOne(WalletTransactions, { foreignKey: 'userId' });
WalletTransactions.belongsTo(Users, { foreignKey: 'userId' });
*/

const userService = {};

// list users based on pagination
userService
  .listUsers = async (page, limit, sort) => {
  if (!page || page < 1) {
    page = 1;
  }

  let offset = 0;
  offset = page - 1;
  offset *= limit;

  const users = await Users
    .findAll({
      offset, limit,
      order: [
        [sort.columnName, sort.order]
      ],
      /*include: [
        Roles, UserDevices, Wallet
      ]*/
    });

  return users;
};


// add new user API
userService
  .addUser = async (payload) => {
  try {
    // add a new user
    const uInfo = {
      fullName: payload.fullName,
      email: payload.email,
    };

    let user = await Users
      .create(payload);
    user = user.dataValues;

    return user;
  } catch (error) {
    return error;
  }
};


module.exports = userService;
